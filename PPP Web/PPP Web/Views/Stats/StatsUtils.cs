﻿using PPP_Web.Models;
using PPP_Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPP_Web.Views.Stats
{
    public class StatsUtils
    {
        public enum School { Högstadiet, Gymnasium }

        public static DateTime[] getDates(DataViewModel data)
        {
            List<DateTime> output = new List<DateTime>();
            var guests = data.Guests;
            foreach (GuestViewModel guest in guests)
            {
                if (!output.Contains(guest.TimeCreated.Date))
                    output.Add(guest.TimeCreated.Date);
            }

            var members = data.MemberLogs;
            foreach (MemberLogViewModel member in members)
            {
                if (!output.Contains(member.EntryTime.Date))
                    output.Add(member.EntryTime.Date);
            }
            output = output.OrderByDescending(d => d.Date).ToList();
            return output.ToArray();
        }

        public static int[] getGuests(DateTime[] dates, bool male, School school, DataViewModel data)
        {
            int[] output = new int[dates.Length];
            var guests = data.Guests;
            var gender = male ? "M" : "F";
            int index = 0;

            foreach (DateTime date in dates)
            {

                int count = 0;
                foreach (GuestViewModel guest in guests)
                {
                    if (guest.TimeCreated.Date.Equals(date) && guest.Gender.Equals(gender) && guest.School.Equals(school.ToString()))
                        count++;
                }
                output[index++] = count;
            }

            return output;
        }

        public static Dictionary<string, char[]> getMemberLogDates(DateTime[] dates, DataViewModel data, out string[] names)
        {
            var output = new Dictionary<string, char[]>();
            var memberLogs = data.MemberLogs;
            List<string> namesList = new List<string>(); 

            foreach (Member member in data.Members)
            {
                if (!namesList.Contains(member.FullName))
                    namesList.Add(member.FullName);
            }

            namesList.Sort();
            foreach (string name in namesList)
            {
                char[] memberPresence = new char[dates.Length];
                int index = 0;

                foreach (DateTime date in dates)
                {
                    
                    if (memberLogs.Any(m => string.Equals(m.Member,name) && m.EntryTime.Date == date))
                        memberPresence[index++] = 'X';
                    else
                        memberPresence[index++] = ' ';
                }
                output.Add(name, memberPresence);
            }
            names = namesList.ToArray();
            return output;
        }
    }
}