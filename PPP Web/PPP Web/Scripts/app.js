﻿var ViewModel = function () {
    var self = this;
    self.guests = ko.observableArray();
    self.error = ko.observable();
    self.detail = ko.observable();
    self.school = ko.observableArray();
    self.gender = ko.observableArray();

    self.newGuest = {
        Gender: ko.observable(),
        School: ko.observable()
    }

    var schoolUri = '/api/schools/';

    function getSchool() {
        ajaxHelper(schoolUri, 'GET').done(function (data) {
            self.school(data);
        });
    }

    self.addGuest = function (formElement) {
        var guest = {
            Gender: self.newGuest.Gender(),
            SchoolId: self.newGuest.SchoolId.Id
        };

        ajaxHelper(guestsUri, 'POST', guest).done(function (item) {
            self.guests.push(item)
        });

        getAllGuests();
    }

    getSchool();

    var guestsUri = '/api/GuestsApi/';

    self.getGuestDetail = function (item) {
        ajaxHelper(guestsUri + item.Id, 'GET').done(function (data) {
            self.detail(data);
        });
    }

    function ajaxHelper(uri, method, data) {
        self.error(''); //Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    function getAllGuests() {
        ajaxHelper(guestsUri, 'GET').done(function (data) {
            
            var gender = [ 'M', 'F' ];

            self.gender(gender);
            self.guests(data);
        });
    }

    getAllGuests();
};

ko.applyBindings(new ViewModel());