package beppebygg.ppp.WebApi;

/**
 * Created by Udvikler on 20-04-2016.
 */
public class Guest {
    public char Gender;
    public int SchoolId;

    public Guest(char gender, int schoolId)
    {
        this.Gender = gender;
        this.SchoolId = schoolId;
    }

    public Guest()
    {

    }
}
