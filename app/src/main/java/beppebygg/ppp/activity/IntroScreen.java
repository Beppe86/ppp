package beppebygg.ppp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.lang.reflect.Member;

import beppebygg.ppp.Members;
import beppebygg.ppp.R;
import beppebygg.ppp.WebApi.WebApiClient;

public class IntroScreen extends AppCompatActivity {


    public void btnGuestClicked(View view)
    {
        Intent openGuestLogin = new Intent(this,GuestLogin.class);
        startActivity(openGuestLogin);
    }

    public void btnMemberClicked(View view)
    {
        if (((Members) this.getApplication()).getMembers() != null) {
            Intent openMemberList = new Intent(this, MemberList.class);
            openMemberList.putExtra("names", ((Members) this.getApplication()).getMembers());
            startActivity(openMemberList);
        } else
        {
            Toast toast = Toast.makeText(this, "Members have not been loaded", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void btnRegisterClicked(View view)
    {
        Intent openRegister = new Intent(this, Register.class);
        startActivity(openRegister);
    }

    public void btnAdminClicked(View view)
    {
//        Intent intent = new Intent(getBaseContext(), DrivePickFolder.class);
//        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        WebApiClient webApi = new WebApiClient();
        if (((Members) this.getApplication()).getMembers() == null)
            webApi.getMembers(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_intro_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setMembers(String[] names)
    {
        ((Members) this.getApplication()).setMembers(names);
    }
}
