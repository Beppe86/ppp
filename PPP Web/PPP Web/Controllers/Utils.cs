﻿using PPP_Web.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace PPP_Web.Controllers
{
    public class Utils
    {
        public static string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        public static HttpResponseMessage HttpResponse(string responseMessage)
        {
                HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.Created);
                responseMsg.Content = new StringContent(responseMessage, System.Text.Encoding.UTF8, "text/plain");
                return responseMsg;
        }

        public static HttpResponseMessage HttpResponse(MemberDTO memberDTO)
        {
            return HttpResponse(String.Format("{0} {1} registrerad", memberDTO.FirstName, memberDTO.LastName));
        }
    }
}