package beppebygg.ppp.driveAbandoned;

import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.OpenFileActivityBuilder;

/**
 * Created by Udvikler on 13-04-2016.
 */
public class DrivePickFolder extends DriveBase {

    private static final String TAG = "PickFolderActivity";

    private static final int REQUEST_CODE_OPENER = 1;

    @Override
    public void onConnected(Bundle connectionHint)
    {
        super.onConnected(connectionHint);
        IntentSender intentSender = Drive.DriveApi
                .newOpenFileActivityBuilder()
                .setMimeType(new String[] {DriveFolder.MIME_TYPE})
                .build(getGoogleApiClient());
        try
        {
            startIntentSenderForResult(intentSender, REQUEST_CODE_OPENER, null, 0, 0, 0);
        } catch (SendIntentException e)
        {
            Log.w(TAG, "Unable to send intent", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_OPENER && resultCode == RESULT_OK)
        {
            DriveId folderId = (DriveId) data.getParcelableExtra(OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID);
            EXISTING_FOLDER_ID = folderId.getResourceId().toString();
            showMessage("Selected folders ID: " +folderId);
            finish();
        }
         else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
