﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using PPP_Web.Models.AuxData;

namespace PPP_Web.ViewModels
{
    public class MemberViewModel
    {
        //Person Info
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }

        public int PostCode { get; set; }
        public string City { get; set; }

        public string BirthDate { get; set; }
        public string Gender { get; set; }

        public byte[] Signature { get; set; }

        public string TimeCreated { get; set; }

        //Optional
        //Contact Info
        public string Email { get; set; }
        public string PhoneNr { get; set; }
        public bool WantInfo { get; set; }

        //ICE
        public string IceName { get; set; }
        public string IceRole { get; set; }
        public string IcePhone { get; set; }


    }
}