package beppebygg.ppp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import beppebygg.ppp.R;
import beppebygg.ppp.WebApi.WebApiClient;

public class ConfirmName extends AppCompatActivity {

    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_name);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        name = getIntent().getStringExtra("name");

        TextView confirmText = ((TextView) (findViewById(R.id.txtConfirmName)));
        String text = confirmText.getText().toString();
        text += " " + name + "?";
        confirmText.setText(text);
    }


    public void btnConfirmClicked(View view)
    {
      WebApiClient.report(name, this);
    }

    public void btnDenyClicked(View view)
    {
        finish();
    }

}
