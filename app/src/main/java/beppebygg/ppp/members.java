package beppebygg.ppp;

import android.app.Application;

/**
 * Created by Udvikler on 09-05-2016.
 */
public class Members extends Application {
    private String[] members;

    public String[] getMembers()
    {
        return members;
    }

    public void setMembers(String[] members)
    {
        this.members = members;
    }
}
