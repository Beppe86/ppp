﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using PPP_Web.Models.AuxData;
using System.ComponentModel.DataAnnotations.Schema;

namespace PPP_Web.Models
{
    public class Member
    {
        public int Id { get; set; }

        //Person Info
        [Index("IX_FirstNameLastName", 1, IsUnique = true)]
        [MaxLength(25)]
        public string FirstName { get; set; }
        [Index("IX_FirstNameLastName", 2, IsUnique = true)]
        [MaxLength(25)]
        public string LastName { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
        
        //Foreign Key
        public int CityId { get; set; }
        // Navigation property
        public virtual City City { get; set; }

        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }

        public string Signature { get; set; }

        public DateTime TimeCreated { get; set; }

        //Optional
        //Contact Info
        public string Email { get; set; }
        public string PhoneNr { get; set; }
        public bool WantInfo { get; set; }

        //ICE
        public string IceName { get; set; }
        public string IceRole { get; set; }
        public string IcePhone { get; set; }

        public Member()
        {
            TimeCreated = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time"));
        }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
