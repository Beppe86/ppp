package beppebygg.ppp.driveAbandoned;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.OpenFileActivityBuilder;


public class DriveCreateFileWithActivity extends DriveBase {

    private static final String TAG = "CreateFileActivity";

//    private static final int REQUEST_CODE_OPENER = 1;
    private static final int REQUEST_CODE_CREATOR = 1;

//    private DriveId mFolderDriveId;

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);

        Drive.DriveApi.newDriveContents(getGoogleApiClient()).setResultCallback(driveContentsCallback);

//        IntentSender intentSender = Drive.DriveApi
//                .newOpenFileActivityBuilder()
//                .setMimeType(new String[] {DriveFolder.MIME_TYPE})
//                .build(getGoogleApiClient());
//
//        try
//        {
//            startIntentSenderForResult(intentSender, REQUEST_CODE_OPENER, null, 0, 0, 0);
//        } catch (IntentSender.SendIntentException e)
//        {
//            Log.w(TAG, "Unable to send intent", e);
//        }
    }

//    final private ResultCallback<DriveIdResult> idCallback = new ResultCallback<DriveIdResult>() {
//        @Override
//        public void onResult(@NonNull DriveIdResult result) {
//            if (!result.getStatus().isSuccess())
//            {
//                showMessage("Cannot find DriveId. Are you authorized to view this file?");
//                return;
//            }
//            mFolderDriveId = result.getDriveId();
//            Drive.DriveApi.newDriveContents(getGoogleApiClient()).setResultCallback(driveContentsCallback);
//        }
//    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            case REQUEST_CODE_CREATOR:
                if (resultCode == RESULT_OK)
                {
                    DriveId driveId = (DriveId) data.getParcelableExtra(OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID);
                    showMessage("Selected folder's ID:" + driveId);
                }
                finish();
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    final private ResultCallback<DriveContentsResult> driveContentsCallback = new ResultCallback<DriveContentsResult>() {
        @Override
        public void onResult(@NonNull DriveContentsResult result) {

            MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
                    .setMimeType("text/html").build();
            IntentSender intentSender = Drive.DriveApi
                    .newCreateFileActivityBuilder()
                    .setInitialMetadata(metadataChangeSet)
                    .setInitialDriveContents(result.getDriveContents())
                    .build(getGoogleApiClient());

            try
            {
                startIntentSenderForResult(intentSender, REQUEST_CODE_CREATOR, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e)
            {
                Log.w(TAG, "Unable to send intent", e);
            }

//            if (!result.getStatus().isSuccess()) {
//                showMessage("Error while trying to create new file contents");
//                return;
//            }
//            final DriveContents driveContents = result.getDriveContents();
//
//            new Thread() {
//                @Override
//                public void run() {
//                    //Write content to DriveContents
//                    OutputStream outputStream = driveContents.getOutputStream();
//                    Writer writer = new OutputStreamWriter(outputStream);
//                    try {
//                        writer.write("Hello PPP");
//                        writer.close();
//                    } catch (IOException e) {
//                        Log.e(TAG, e.getMessage());
//                    }
//
//                    DriveFolder folder = mFolderDriveId.asDriveFolder();
//                    MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
//                            .setTitle("New file")
//                            .setMimeType("text/plain")
//                            .setStarred(true).build();
//                    folder.createFile(getGoogleApiClient(), changeSet, result.getDriveContents()).setResultCallback(fileCallback);
//                }
//            }.start();
        }
    };

//
//    final private ResultCallback<DriveFileResult> fileCallback = new
//            ResultCallback<DriveFileResult>() {
//                @Override
//                public void onResult(DriveFileResult result) {
//                    if (!result.getStatus().isSuccess()) {
//                        showMessage("Error while trying to create the file");
//                        return;
//                    }
//                    showMessage("Created a file with content: "
//                            + result.getDriveFile().getDriveId());
//                }
//            };
}
