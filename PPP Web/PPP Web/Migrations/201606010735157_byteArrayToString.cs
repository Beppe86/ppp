namespace PPP_Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class byteArrayToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Members", "Signature", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Members", "Signature", c => c.Binary());
        }
    }
}
