﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PPP_Web.Repository;
using AutoMapper;
using PPP_Web.ViewModels;
using PPP_Web.Models;
using System.Net;

namespace PPP_Web.Controllers.APIs
{
    public class GuestsApiController : ApiController
    {
        private readonly IGenericRepository<Guest> _guestRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GuestsApiController(IMapper mapper, IUnitOfWork unitOfWork, IGenericRepository<Guest> guestRepository)
        {
            _mapper = mapper;
            _guestRepository = guestRepository;
            _unitOfWork = unitOfWork;
        }

        // GET: api/Guests
        public IQueryable<GuestViewModel> GetGuests()
        {
            //var gduests = from g in db.Guests
            //             select new GuestDTO()
            //             {
            //                 Gender = g.Gender,
            //                 School = g.School.Name,
            //                 TimeCreated = g.TimeCreated.ToString()
            //             };

            var guests = from g in _guestRepository.AsQueryable() 
                         select new GuestViewModel()
                         {
                             Gender = g.Gender,
                             School = g.School.Name,
                             TimeCreated = g.TimeCreated
                         };

            return guests;
        }

        #region unused

        //// GET: api/Guests/5
        //[ResponseType(typeof(Guest))]
        //public async Task<IHttpActionResult> GetGuest(int id)
        //{
        //    Guest guest = await _guestRepository.GetByKeyAsync(id);
        //    if (guest == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(guest);
        //}

        //// PUT: api/Guests/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutGuest(int id, Guest guest)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != guest.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _guestRepository.Update(guest);

        //    try
        //    {
        //        await _unitOfWork.SaveAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!GuestExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        #endregion unused

        // POST: api/Guests
        [ResponseType(typeof(Guest))]
        public async Task<IHttpActionResult> PostGuest(Guest guest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (guest == null)
            {
                return StatusCode(HttpStatusCode.NotAcceptable);
            }

            _guestRepository.Insert(guest);
            await _unitOfWork.SaveAsync();

            IHttpActionResult response;
            response = ResponseMessage(Utils.HttpResponse("Välkommen"));
            return response;
        }

        #region unused

        //// DELETE: api/Guests/5
        //[ResponseType(typeof(Guest))]
        //public async Task<IHttpActionResult> DeleteGuest(int id)
        //{
        //    Guest guest = await _guestRepository.GetByKeyAsync(id);
        //    if (guest == null)
        //    {
        //        return NotFound();
        //    }

        //    _guestRepository.DeleteByKey(guest);
        //    await _unitOfWork.SaveAsync();

        //    return Ok(guest);
        //}

        //private bool GuestExists(int id)
        //{
        //    return _guestRepository.Count(e => e.Id == id) > 0;
        //}

        #endregion unused
    }
}