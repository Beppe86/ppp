﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using PPP_Web.Models.AuxData;

namespace PPP_Web.Models
{
    public class Guest
    {
        public int Id { get; set; }
        public string Gender { get; set; }
        public DateTime TimeCreated { get; set; }

        //Foreign Key
        public int SchoolId { get; set; }
        // Navigation property
        public virtual School School { get; set; }

        public Guest()
        {
            TimeCreated = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time"));
        }
    }
}