﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PPP_Web.Models;
using PPP_Web.Repository;
using AutoMapper;
using System.Web.Http.Description;
using PPP_Web.ViewModels;
using PPP_Web.Models.AuxData;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace PPP_Web.Controllers
{
    public class StatsController : Controller
    {
        private PPP_WebContext db = new PPP_WebContext();
        private readonly IGenericRepository<Guest> _guestRepository;
        private readonly IGenericRepository<MemberLog> _memberLogRepository;
        private readonly IGenericRepository<Member> _memberRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public StatsController(IMapper mapper, IUnitOfWork unitOfWork, IGenericRepository<MemberLog> memberLogRepository, IGenericRepository<Guest> guestRepository, IGenericRepository<Member> memberRepository)
        {
            this._guestRepository = guestRepository;
            this._memberLogRepository = memberLogRepository;
            this._memberRepository = memberRepository;
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        // GET: Stats
        public ActionResult Index() //How to make async?
        {
            var guests = _guestRepository.AsQueryable();
            var guestsVm = _mapper.Map<List<GuestViewModel>>(guests);
            var memberLogs = _memberLogRepository.AsQueryable();
            var memberlogsVm = _mapper.Map<List<MemberLogViewModel>>(memberLogs);
            var members = _memberRepository.AsQueryable();
            var membersVm = _mapper.Map<List<Member>>(members);

            var data = new DataViewModel
            {
                Guests = guestsVm,
                MemberLogs = memberlogsVm,
                Members = membersVm
            };

            return View(data);
        }

        // GET: Stats/Details/5
        public async Task<ActionResult> Details(string fullname)
        {
            if (fullname == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var member = _memberRepository.AsQueryable().FirstOrDefault(x => string.Equals(x.FirstName + " " + x.LastName, fullname));
            if (member == null)
            {
                return HttpNotFound();
            }
            if (member.Signature != null)
            {
                //var assembly = System.Reflection.Assembly.GetExecutingAssembly();
                //using (var stream = assembly.GetManifestResourceStream("PPP_Web.Images.sample.bmp"))
                //{
                //    byte[] buffer = new byte[stream.Length];
                //    stream.Read(buffer, 0, buffer.Length);
                //    ViewBag.ImageData = buffer;
                //}
                //MemoryStream ms = new MemoryStream();
                //Bitmap bmp = new Bitmap(System.Reflection.Assembly.GetEntryAssembly().GetManifestResourceStream("PPP_Web.Images.sample.bmp"));
                //bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                ////ViewBag.ImageData = ms.ToArray();
                ViewBag.ImageData = member.Signature;
            }
            return View(member);
        }

        // GET: Stats/Create
        public ActionResult Create()
        {
            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "Name");
            return View();
        }

        // POST: Stats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Gender,TimeCreated,SchoolId")] Guest guest)
        {
            if (ModelState.IsValid)
            {
                db.Guests.Add(guest);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.SchoolId = new SelectList(db.Schools, "Id", "Name", guest.SchoolId);
            return View(guest);
        }

        // GET: Stats/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = await _memberRepository.GetByKeyAsync(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.Cities, "Id", "Name", member.CityId);
            return View(member);
        }

        // POST: Stats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,FirstName,LastName,Address,PostCode,CityId,BirthDate,Gender,Signature,TimeCreated,Email,PhoneNr,WantInfo,IceName,IceRole,IcePhone")] Member member)
        {
            if (ModelState.IsValid)
            {
                School newSchool = new School { Name = member.City.Name };
                db.Entry(member).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.Cities, "Id", "Name", member.CityId);
            return View(member);
        }

        // GET: Stats/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guest guest = await db.Guests.FindAsync(id);
            if (guest == null)
            {
                return HttpNotFound();
            }
            return View(guest);
        }

        // POST: Stats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Guest guest = await db.Guests.FindAsync(id);
            db.Guests.Remove(guest);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
