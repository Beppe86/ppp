package beppebygg.ppp.WebApi;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;

import beppebygg.ppp.activity.IntroScreen;
import beppebygg.ppp.activity.LoginResult;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Udvikler on 20-04-2016.
 */
public class WebApiClient {

    private IntroScreen introScreen;

    private static GitApiInterface gitApiInterface;
    public static final String API_BASE_URL = "http://pppapp.azurewebsites.net";
    public static final String API_LOCAL_URL = "http://mylolapp:53046/";

    public static GitApiInterface getClient() {
        if (gitApiInterface == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okClient = new OkHttpClient.Builder()
                    .addInterceptor(
                            new Interceptor() {
                                @Override
                                public Response intercept(Chain chain) throws IOException {
                                    return chain.proceed(chain.request());
                                }
                            }).addInterceptor(logging).build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .client(okClient)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }

        return gitApiInterface;
    }

    public interface GitApiInterface {
        @POST("api/GuestsApi")
        Call<String> createGuest(@Body Guest guest);

        @POST("api/MembersApi")
        Call<String> createMember(@Body Registered registered);

        @Headers("Content-Type: application/json")
        @POST("api/MemberLogsApi")
        Call<String> reportMemberPresence(@Body String  name);

        @GET("api/MembersApi")
        Call<String[]>getMembers();
    }


    public static void report(final String name, final Context context)
    {
        openLoginActivity(context);
        WebApiClient.GitApiInterface service = WebApiClient.getClient();

        Call<String> call = service.reportMemberPresence("\"" + name + "\"");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                Log.d("Guest Login", "Status Code" + response.code());

                openLoginActivity(context, response.isSuccessful(), LoginResult.Type.MEMBER, name);

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Guest Login", "Failed");
            }
        });
    }


    public static void report(final Guest guest, final Context context) {

        openLoginActivity(context);
        WebApiClient.GitApiInterface service = WebApiClient.getClient();

        Call<String> call = service.createGuest(guest);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                Log.d("Guest Login", "Status Code" + response.code());
                openLoginActivity(context, response.isSuccessful(), LoginResult.Type.GUEST, guest);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Guest Login", "Failed");
            }
        });
    }

    public static void report(final Registered registered, final Context context)
    {
        openLoginActivity(context);
        WebApiClient.GitApiInterface service = WebApiClient.getClient();

        Call<String> call = service.createMember(registered);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                Log.d("Guest Login", "Status Code" + response.code());
                openLoginActivity(context, response.isSuccessful(), LoginResult.Type.REGISTERED, registered);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("Guest Login", "Failed");
            }
        });
    }

    public void getMembers(IntroScreen intro)
    {
        this.introScreen = intro;
        WebApiClient.GitApiInterface service = WebApiClient.getClient();

        Call<String[]> call = service.getMembers();
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, retrofit2.Response<String[]> response) {
                Log.d("Guest Login", "Status Code" + response.code());
                if (response.isSuccessful()) {
                    introScreen.setMembers(response.body());
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                Log.d("getMembers", "Error");
            }
        });
    }

    private static void openLoginActivity(Context context, boolean status, LoginResult.Type type, Object toSend) {
        Intent loginReport = new Intent(context, LoginResult.class);
        loginReport.putExtra("type", type);

        if (type != LoginResult.Type.DEFAULT) {
            loginReport.putExtra("status", status);

            if (toSend.getClass() == Registered.class)
                loginReport.putExtra("registered", (Registered) toSend);

            else if (toSend.getClass() == Guest.class) {
                Guest guest = (Guest) toSend;
                loginReport.putExtra("guest", String.valueOf(guest.Gender) + guest.SchoolId);
            } else
                loginReport.putExtra("member", toSend.toString());
        }


        loginReport.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        loginReport.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(loginReport);
    }

    private static void openLoginActivity(Context context)
    {
        openLoginActivity(context, false, LoginResult.Type.DEFAULT, null);
    }
}
