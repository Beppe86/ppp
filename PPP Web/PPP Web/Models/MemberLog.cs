﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPP_Web.Models
{
    public class MemberLog
    {
        public int Id { get; set; }
        public DateTime EntryTime { get; set; }

        //Foreign Key
        public int MemberId { get; set; }
        // Navigation property
        public virtual Member Member { get; set; }

        public MemberLog(int memberId)
        {
            EntryTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time"));
            this.MemberId = memberId;
        }

        public MemberLog()
        {
            EntryTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time"));
        }
    }
}