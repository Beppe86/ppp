package beppebygg.ppp.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kyanogen.signatureview.SignatureView;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;

import beppebygg.ppp.LockableScollView;
import beppebygg.ppp.R;
import beppebygg.ppp.WebApi.Registered;

public class Register extends AppCompatActivity {

    private Registered reg = new Registered();
    private SignatureView signature;
    private View background;
    private Button btnClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        signature = (SignatureView) findViewById(R.id.signature);
        background = findViewById(R.id.backgroundLayout);
//        btnClear = (Button) findViewById(R.id.clear);
//
//        btnClear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                signature.clearCanvas();
//            }
//        });

        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((LockableScollView) findViewById(R.id.scrollView)).setScrollingEnabled(false);
                return false;
            }
        });

        background.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((LockableScollView) findViewById(R.id.scrollView)).setScrollingEnabled(true);
                return true;
            }
        });
    }

    public void btnClear(View view)
    {
        signature.clearCanvas();
    }

    public void btnRegister(View view) {
        if (validateForm()) {
            Intent openIce = new Intent(this, Ice.class);
            openIce.putExtra("registered", reg);
            startActivity(openIce);
        }
    }

    private boolean validateForm() {

        if (((RadioGroup) findViewById(R.id.genderRadioRegi)).getCheckedRadioButtonId() == -1)
            return false;

        boolean formOK = true;

        reg.FirstName = ((EditText) findViewById(R.id.txtFirstName)).getText().toString();
        reg.LastName = ((EditText) findViewById(R.id.txtLastName)).getText().toString();
        reg.Address = ((EditText) findViewById(R.id.txtAdress)).getText().toString();
        reg.City = ((EditText) findViewById(R.id.txtCity)).getText().toString();

        Bitmap img = ((SignatureView) findViewById(R.id.signature)).getSignatureBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.PNG, 0, stream);
        reg.Signature = new String(Base64.encode(stream.toByteArray(), 0));




        String postNr = ((EditText) findViewById(R.id.txtPostalNbr)).getText().toString();
        if (postNr.length() > 0)
            reg.PostCode = Integer.parseInt(postNr);
        else
            return false;

        reg.BirthDate = ((Date)getDateFromDatePicker((DatePicker) findViewById(R.id.dateBirth))).getTime();
        reg.Gender = ((RadioButton) findViewById(R.id.radioBoyRegi)).isChecked() ? 'M' : 'F';

        //Optional
        reg.Email = ((EditText) findViewById(R.id.txtEmail)).getText().toString();
        reg.PhoneNr = ((EditText) findViewById(R.id.txtPhone)).getText().toString();
        reg.WantInfo = ((CheckBox) findViewById(R.id.checkConsentInfo)).isChecked();

        formOK = (formOK && !reg.FirstName.isEmpty());
        formOK = (formOK && !reg.LastName.isEmpty());
        formOK = (formOK && !reg.Address.isEmpty());
        formOK = (formOK && String.valueOf(reg.PostCode).length() > 0);
        formOK = (formOK && !reg.City.isEmpty());

        return formOK;
    }

    private Date getDateFromDatePicker(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }
}
