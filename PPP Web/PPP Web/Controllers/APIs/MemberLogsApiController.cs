﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PPP_Web.Models;
using PPP_Web.Repository;
using AutoMapper;

namespace PPP_Web.Controllers.APIs
{
    public class MemberLogsApiController : ApiController
    {
        private readonly IGenericRepository<MemberLog> _memberLogRepository;
        private readonly IGenericRepository<Member> _memberRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public MemberLogsApiController(IMapper mapper, IUnitOfWork unitOfWork, IGenericRepository<MemberLog> memberLog, IGenericRepository<Member> member)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _memberLogRepository = memberLog;
            _memberRepository = member;
        }

        #region unused

        //// GET: api/MemberLogs
        //public IQueryable<MemberLog> GetMemberLogs()
        //{
        //    return _memberLogRepository.AsQueryable();
        //}

        //// GET: api/MemberLogs/5
        //[ResponseType(typeof(MemberLog))]
        //public async Task<IHttpActionResult> GetMemberLog(int id)
        //{
        //    MemberLog memberLog = await _memberLogRepository.GetByKeyAsync(id);
        //    if (memberLog == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(memberLog);
        //}

        // PUT: api/MemberLogs/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutMemberLog(int id, MemberLog memberLog)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != memberLog.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(memberLog).State = EntityState.Modified;

        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!MemberLogExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        #endregion unused

        // POST: api/MemberLogs
        [ResponseType(typeof(MemberLog))]
        public async Task<IHttpActionResult> PostMemberLog([FromBody]string name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (name == null)
            {
                return StatusCode(HttpStatusCode.NotAcceptable);
            }

            var firstName = name.Split(' ')[0];
            var lastName = name.Split(' ')[1];

            var member = _memberRepository.AsQueryable().First(x => x.FirstName.Equals(firstName) && x.LastName.Equals(lastName));
            _memberLogRepository.Insert(new MemberLog(member.Id));
            await _unitOfWork.SaveAsync();

            IHttpActionResult response;
            HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.Created);
            responseMsg.Content = new StringContent("Välkommen");
            response = ResponseMessage(responseMsg);
            return response;
        }

        #region unused

        //// DELETE: api/MemberLogs/5
        //[ResponseType(typeof(MemberLog))]
        //public async Task<IHttpActionResult> DeleteMemberLog(int id)
        //{
        //    MemberLog memberLog = await db.MemberLogs.FindAsync(id);
        //    if (memberLog == null)
        //    {
        //        return NotFound();
        //    }

        //    db.MemberLogs.Remove(memberLog);
        //    await db.SaveChangesAsync();

        //    return Ok(memberLog);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool MemberLogExists(int id)
        //{
        //    return db.MemberLogs.Count(e => e.Id == id) > 0;
        //}

        #endregion unused
    }
}