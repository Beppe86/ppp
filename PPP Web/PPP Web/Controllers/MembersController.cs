﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PPP_Web.Models;

namespace PPP_Web.Controllers
{
    public class MembersController : Controller
    {
        private PPP_WebContext db = new PPP_WebContext();

        // GET: Members
        public ActionResult Index()
        {
            var members = db.Members.Include(m => m.City);
            return View(members.ToList());
        }

        // GET: Members/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // GET: Members/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.Cities, "Id", "Name");
            return View();
        }

        // POST: Members/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Address,PostCode,CityId,BirthDate,Gender,Signature,TimeCreated,Email,PhoneNr,WantInfo,IceName,IceRole,IcePhone")] Member member)
        {
            if (ModelState.IsValid)
            {
                db.Members.Add(member);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CityId = new SelectList(db.Cities, "Id", "Name", member.CityId);
            return View(member);
        }

        // GET: Members/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.Cities, "Id", "Name", member.CityId);
            return View(member);
        }

        // POST: Members/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Exclude = "BirthDate, TimeCreated, Signature", Include = "Id,FirstName,LastName,Address,PostCode,CityId,Gender,Email,PhoneNr,WantInfo,IceName,IceRole,IcePhone")] Member updatedMember)
        {
            if (ModelState.IsValid)
            {
                var oldMember = db.Members.AsQueryable().FirstOrDefault(m => m.Id == updatedMember.Id);
                oldMember.FirstName = updatedMember.FirstName;
                oldMember.LastName = updatedMember.LastName;
                oldMember.Address = updatedMember.Address;
                oldMember.PostCode = updatedMember.PostCode;
                oldMember.CityId = updatedMember.CityId;
                oldMember.Gender = updatedMember.Gender;
                oldMember.Email = updatedMember.Email;
                oldMember.PhoneNr = updatedMember.PhoneNr;
                oldMember.WantInfo = updatedMember.WantInfo;
                oldMember.IceName = updatedMember.IceName;
                oldMember.IceRole = updatedMember.IceRole;
                oldMember.IcePhone = updatedMember.IcePhone;

                db.Entry(oldMember).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", "Stats", new { oldMember.FullName});
            }
            ViewBag.CityId = new SelectList(db.Cities, "Id", "Name", updatedMember.CityId);
            return View(updatedMember);
        }

        // GET: Members/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // POST: Members/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Member member = db.Members.Find(id);
            db.Members.Remove(member);
            db.SaveChanges();
            return RedirectToAction("Index", "Stats");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
