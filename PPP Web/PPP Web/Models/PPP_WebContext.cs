﻿using Microsoft.AspNet.Identity.EntityFramework;
using PPP_Web.Models.AuxData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PPP_Web.Models
{
    public class PPP_WebContext : IdentityDbContext<ApplicationUser>
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public PPP_WebContext()
            : base("PPPAPP")
        {
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public static PPP_WebContext Create()
        {
            return new PPP_WebContext();
        }

        public DbSet<Guest> Guests { get; set; }

        public DbSet<School> Schools { get; set; }

        public DbSet<Member> Members { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<MemberLog> MemberLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Member>().Property(x => x.FirstName).IsRequired();
        }
    }
}
