﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PPP_Web.Startup))]
namespace PPP_Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
