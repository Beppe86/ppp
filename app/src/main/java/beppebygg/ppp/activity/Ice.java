package beppebygg.ppp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import beppebygg.ppp.R;
import beppebygg.ppp.WebApi.Registered;
import beppebygg.ppp.WebApi.WebApiClient;

public class Ice extends AppCompatActivity {

    Registered registered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ice);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        registered = getIntent().getParcelableExtra("registered");

    }

    public void btnConfirm(View view) {

        EditText ice = (EditText) findViewById(R.id.txtIcePhone);

        if (ice.getText().length() > 6) { //checks phone number length first
            registered.IcePhone = ice.getText().toString();
            ice = (EditText) findViewById(R.id.txtIceName);
            registered.IceName = ice.getText().toString();
            ice = (EditText) findViewById(R.id.txtRole);
            registered.IceRole = ice.getText().toString();
        }

        WebApiClient.report(registered, this);
        finish();
    }
}
