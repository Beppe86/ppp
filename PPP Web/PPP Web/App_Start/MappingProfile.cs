﻿using AutoMapper;
using PPP_Web.Models;
using PPP_Web.Models.AuxData;
using PPP_Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPP_Web.App_Start
{
    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<MemberDTO, Member>()
                //Ignore the IDs
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CityId, opt => opt.Ignore())
                .ForMember(dest => dest.City, opt => opt.Ignore())
                .ForMember(dest => dest.TimeCreated, opt => opt.Ignore())

                //Formats names
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => Controllers.Utils.FirstLetterToUpper(src.FirstName)))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => Controllers.Utils.FirstLetterToUpper(src.LastName)))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => Controllers.Utils.FirstLetterToUpper(src.Address)))

                .ForMember(dest => dest.IceName, opt => opt.MapFrom(src => Controllers.Utils.FirstLetterToUpper(src.IceName)))
                .ForMember(dest => dest.IceRole, opt => opt.MapFrom(src => Controllers.Utils.FirstLetterToUpper(src.IceRole)))

                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => MillisecTimeToDateTime(src.BirthDate)))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Signature, opt => opt.MapFrom(src => src.Signature));

            CreateMap<Guest, GuestViewModel>()
                .ForMember(dest => dest.School, opt => opt.MapFrom(src => src.School.Name))
                .ReverseMap();

            CreateMap<MemberLog, MemberLogViewModel>()
                .ForMember(dest => dest.Member, opt => opt.MapFrom(src => src.Member.FullName))
                .ReverseMap();


            Mapper.AssertConfigurationIsValid();
        }

        private DateTime MillisecTimeToDateTime(Int64 milliseconds)
        {
            TimeSpan ss = TimeSpan.FromMilliseconds(milliseconds);
            DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime ddd = Jan1st1970.Add(ss);
            return ddd.ToUniversalTime();
        }
    }
}
