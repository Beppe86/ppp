﻿using PPP_Web.Models;
using PPP_Web.Models.AuxData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPP_Web.ViewModels
{
    public class DataViewModel
    {
        public List<GuestViewModel> Guests { get; set; }
        public List<MemberLogViewModel> MemberLogs { get; set; }
        public List<Member> Members { get; set; }
    }

    public class MemberLogViewModel
    {
        public DateTime EntryTime { get; set; }
        public string Member { get; set; }
    }

    public class GuestViewModel
    {
        public DateTime TimeCreated { get; set; }
        public string Gender { get; set; }
        public string School { get; set; }
    }
}