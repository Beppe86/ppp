package beppebygg.ppp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import beppebygg.ppp.R;
import beppebygg.ppp.WebApi.Guest;
import beppebygg.ppp.WebApi.WebApiClient;

public class GuestLogin extends AppCompatActivity {

    public void btnGuestLogin(View view) {

        Guest guest = new Guest();

        RadioGroup grp = (RadioGroup) findViewById(R.id.genderRadio);
        RadioButton gender = (RadioButton) findViewById(R.id.radioBoy);
        RadioButton school = (RadioButton) findViewById(R.id.radioHighSchool);

        int selectedId = grp.getCheckedRadioButtonId();

        if (selectedId == -1)
            return;

        grp = (RadioGroup) findViewById(R.id.radioGrpSchool);
        selectedId = grp.getCheckedRadioButtonId();

        if (selectedId == -1)
            return;

        guest.Gender = (gender.isChecked()) ? 'M' : 'F';
        guest.SchoolId = (school.isChecked()) ? 2 : 1;


            WebApiClient.report(guest, this);
            finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }
}
