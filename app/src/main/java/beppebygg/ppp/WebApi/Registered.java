package beppebygg.ppp.WebApi;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import beppebygg.ppp.activity.Register;

/**
 * Created by Udvikler on 21-04-2016.
 */
public class Registered implements Parcelable {

    public String FirstName;
    public String LastName;
    public String Address;
    public int PostCode;
    public String City;

    public long BirthDate;
    public char Gender;

    public String Email;
    public String PhoneNr;
    public boolean WantInfo;

    public String Signature;

    //Ice
    public String IceName;
    public String IceRole;
    public String IcePhone;

    public Registered()
    {

    }

    protected Registered(Parcel in) {
        FirstName = in.readString();
        LastName = in.readString();
        Address = in.readString();
        PostCode = in.readInt();
        City = in.readString();
        BirthDate = in.readLong();
        Gender = (char) in.readValue(char.class.getClassLoader());
        Email = in.readString();
        PhoneNr = in.readString();
        WantInfo = in.readByte() != 0x00;
        Signature = in.readString();
        IceName = in.readString();
        IceRole = in.readString();
        IcePhone = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(FirstName);
        dest.writeString(LastName);
        dest.writeString(Address);
        dest.writeInt(PostCode);
        dest.writeString(City);
        dest.writeLong(BirthDate);
        dest.writeValue(Gender);
        dest.writeString(Email);
        dest.writeString(PhoneNr);
        dest.writeByte((byte) (WantInfo ? 0x01 : 0x00));
        dest.writeString(Signature);
        dest.writeString(IceName);
        dest.writeString(IceRole);
        dest.writeString(IcePhone);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Registered> CREATOR = new Parcelable.Creator<Registered>() {
        @Override
        public Registered createFromParcel(Parcel in) {
            return new Registered(in);
        }

        @Override
        public Registered[] newArray(int size) {
            return new Registered[size];
        }
    };
}