package beppebygg.ppp.driveAbandoned;

import java.io.IOException;
import java.io.OutputStream;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveApi.DriveIdResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;

/**
 * Created by Udvikler on 14-04-2016.
 */
public class DriveEditFile extends DriveBase {

    private static final String TAG = "EditFileActivity";

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);

        final ResultCallback<DriveIdResult> idCallback = new ResultCallback<DriveIdResult>() {
            @Override
            public void onResult(@NonNull DriveIdResult result) {
                if (!result.getStatus().isSuccess())
                {
                    showMessage("Cannot find DriveId. Are you authorized to view this file?");
                    return;
                }

                DriveId driveId = result.getDriveId();
                DriveFile file = driveId.asDriveFile();
                new EditContentsAsyncTask(DriveEditFile.this).execute(file);
            }
        };
        Drive.DriveApi.fetchDriveId(getGoogleApiClient(), EXISTING_FILE_ID).setResultCallback(idCallback);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        super.onConnectionFailed(connectionResult);
    }

    public class EditContentsAsyncTask extends ApiClientAsyncTask<DriveFile, Void, Boolean>
    {
        public EditContentsAsyncTask(Context context)
        {
            super(context);
        }

        @Override
        protected Boolean doInBackgroundConnected(DriveFile... params) {
            DriveFile file = params[0];
            try
            {
                DriveContentsResult driveContentsResult = file.open(getGoogleApiClient(), DriveFile.MODE_WRITE_ONLY, null).await();
                if (!driveContentsResult.getStatus().isSuccess()) {
                    return false;
                }
                DriveContents driveContents = driveContentsResult.getDriveContents();
                OutputStream outputStream = driveContents.getOutputStream();
                outputStream.write("Hello world".getBytes());
                com.google.android.gms.common.api.Status status = driveContents.commit(getGoogleApiClient(), null).await();
                return status.getStatus().isSuccess();
            } catch (IOException e) {
                Log.e(TAG, "IOException while appending to the output stream", e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) {
                showMessage("Error while editing contents");
                return;
            }
            showMessage("Successfully edited contents");
        }
    }
}
