package beppebygg.ppp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import beppebygg.ppp.R;
import beppebygg.ppp.WebApi.WebApiClient;

public class MemberList extends AppCompatActivity {

    private String[] names;
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        names = getIntent().getStringArrayExtra("names");

        list = (ListView) findViewById(R.id.listMembers);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openConfirmActivity(position);
            }
        });
    }

    private void openConfirmActivity(int position) {
        Intent confirmName = new Intent(this, ConfirmName.class);
        confirmName.putExtra("name", names[position]);
        startActivity(confirmName);
    }
}
