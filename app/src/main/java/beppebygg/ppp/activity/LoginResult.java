package beppebygg.ppp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import beppebygg.ppp.R;
import beppebygg.ppp.WebApi.Guest;
import beppebygg.ppp.WebApi.Registered;
import beppebygg.ppp.WebApi.WebApiClient;

public class LoginResult extends AppCompatActivity {

    private Guest guest;
    private Registered registered;
    private String name;

    public enum Type {
        ERROR, DEFAULT, GUEST, REGISTERED, MEMBER
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_success);

        String statusText = getString(R.string.login_trying);
        Type type = (Type) getIntent().getSerializableExtra("type");

        if (type != Type.DEFAULT) {
            boolean status = getIntent().getBooleanExtra("status", false);

            if (type == Type.ERROR)
            {
                Log.e("type", "error, type not valid");
            }

            if (status) {
                Button btnReturn = (Button) findViewById(R.id.btnReturnToHome);
                btnReturn.setEnabled(status);
                btnReturn.setAlpha(1);
                statusText = getString(R.string.login_success);
            } else {
                Button btnRetry = (Button) findViewById(R.id.btnRetry);
                btnRetry.setEnabled(!status);
                btnRetry.setAlpha(1);
                statusText = getString(R.string.login_failed);

                switch (type) {
                    case GUEST:
                        String guestInfo = getIntent().getStringExtra("guest");
                        guest = new Guest(guestInfo.charAt(0), Integer.parseInt(String.valueOf(guestInfo.charAt(1))));
                        registered = null;
                        name = null;
                        break;

                    case REGISTERED:
                        registered = getIntent().getParcelableExtra("registered");
                        guest = null;
                        name = null;
                        break;

                    case MEMBER:
                        name = getIntent().getStringExtra("member");
                        guest = null;
                        registered = null;
                        break;
                }
            }
        }

        ((TextView) findViewById(R.id.txtLoginReport)).setText(statusText);
    }

    public void btnReturnClicked(View view)
    {
        Intent intent = new Intent(this, IntroScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    public void btnRetryClicked(View view)
    {
        if (guest != null)
            WebApiClient.report(guest, this);
        if (registered != null)
            WebApiClient.report(registered, this);
        if (name != null)
            WebApiClient.report(name, this);
    }

}
