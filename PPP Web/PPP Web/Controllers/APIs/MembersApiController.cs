﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PPP_Web.Models;
using PPP_Web.Repository;
using AutoMapper;
using PPP_Web.Models.AuxData;

namespace PPP_Web.Controllers.APIs
{
    public class MembersApiController : ApiController
    {
        private readonly IGenericRepository<City> _cityRepository;
        private readonly IGenericRepository<Member> _memberRepository;
        private readonly IGenericRepository<MemberLog> _memberLogRepository;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public MembersApiController(IMapper mapper, IUnitOfWork unitOfWork, IGenericRepository<Member> memberRepository, IGenericRepository<City> cityRepository, IGenericRepository<MemberLog> memberLogRepository)
        {
            _mapper = mapper;
            _memberRepository = memberRepository;
            _unitOfWork = unitOfWork;
            _cityRepository = cityRepository;
            _memberLogRepository = memberLogRepository;
        }
    
        // GET: api/Members
        [AllowAnonymous]
        public List<string> GetMembers()
        {
            return _memberRepository.AsQueryable().Select(x => x.FirstName + " " + x.LastName).ToList();
        }

        #region unused

        //// GET: api/Members/5
        //[ResponseType(typeof(Member))]
        //public async Task<IHttpActionResult> GetMember(int id)
        //{
        //    Member member = await _memberRepository.GetByKeyAsync(id);
        //    if (member == null)
        //    {
        //        return NotFound();
        //    }



        //    return Ok(member);
        //}

        //// PUT: api/Members/5
        //[HttpPut, ActionName("Edit")]
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutMember(Member member)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    //if (memid != member.Id)
        //    //{
        //    //    return BadRequest();
        //    //}

        //    _memberRepository.Update(member);

        //    try
        //    {

        //        await _unitOfWork.SaveAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!MemberExists(member.Id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        #endregion unused

        // POST: api/Members
        [AllowAnonymous]
        [ResponseType(typeof(Member))]
        public async Task<IHttpActionResult> PostMember(MemberDTO memberDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (memberDTO == null)
            {
                return StatusCode(HttpStatusCode.NotAcceptable);
            }

            //Check if the named city exists
            var city = _cityRepository.AsQueryable().FirstOrDefault(c => c.Name.ToLower() == memberDTO.City.ToLower());
            if (city == null)
                city = new City() { Name = Utils.FirstLetterToUpper(memberDTO.City) };

            var member = _mapper.Map<Member>(memberDTO);
            member.City = city;

            //checks for duplicates
            var nameAlreadyExists = _memberRepository.AsQueryable().Any(x => x.FirstName == memberDTO.FirstName && x.LastName == memberDTO.LastName);
            IHttpActionResult response;
            
            if (!nameAlreadyExists)
            {
                _memberRepository.Insert(member);
                _memberLogRepository.Insert(new MemberLog(member.Id));
                await _unitOfWork.SaveAsync();

                HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.Created);
                responseMsg.Content = new StringContent(String.Format("{0} {1} registrerad", memberDTO.FirstName, memberDTO.LastName));
                response = ResponseMessage(responseMsg);
                return response;
            }
            else
            {
                HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.Conflict);
                response = ResponseMessage(responseMsg);
                return response;
            }
        }

        #region unused

        //// DELETE: api/Members/5
        //[ResponseType(typeof(Member))]
        //public async Task<IHttpActionResult> DeleteMember(int id)
        //{
        //    Member member = await _memberRepository.GetByKeyAsync(id);

        //    if (member == null)
        //    {
        //        return NotFound();
        //    }

        //    _memberRepository.DeleteByKey(member);
        //    await _unitOfWork.SaveAsync();

        //    return Ok(member);
        //}

        //private bool MemberExists(int id)
        //{
        //    return _memberRepository.Count(e => e.Id == id) > 0;
        //}

        #endregion unused
    }
}