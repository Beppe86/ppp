﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PPP_Web;
using PPP_Web.Models;
using PPP_Web.Repository;
using AutoMapper;
using PPP_Web.Models.AuxData;

namespace PPP_Web.Controllers
{
    public class SchoolsController : ApiController
    {
        private readonly IGenericRepository<School> _schoolRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public SchoolsController(IMapper mapper, IUnitOfWork unitOfWork, IGenericRepository<School> schoolRepository)
        {
            _mapper = mapper;
            _schoolRepository = schoolRepository;
            _unitOfWork = unitOfWork;
        }

        // GET: api/Schools
        public IQueryable<School> GetSchools()
        {
            return _schoolRepository.AsQueryable();
        }

        // GET: api/Schools/5
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> GetSchool(int id)
        {
            School school = await _schoolRepository.GetByKeyAsync(id);
            if (school == null)
            {
                return NotFound();
            }

            return Ok(school);
        }

        // PUT: api/Schools/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSchool(int id, School school)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != school.Id)
            {
                return BadRequest();
            }

            _schoolRepository.Update(school);

            try
            {

                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SchoolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Schools
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> PostSchool(School school)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _schoolRepository.Insert(school);
            await _unitOfWork.SaveAsync();

            return CreatedAtRoute("DefaultApi", new { id = school.Id }, school);
        }

        // DELETE: api/Schools/5
        [ResponseType(typeof(School))]
        public async Task<IHttpActionResult> DeleteSchool(int id)
        {
            School school = await _schoolRepository.GetByKeyAsync(id);

            if (school == null)
            {
                return NotFound();
            }

            _schoolRepository.DeleteByKey(school);
            await _unitOfWork.SaveAsync();

            return Ok(school);
        }

        private bool SchoolExists(int id)
        {
            return _schoolRepository.Count(e => e.Id == id) > 0;
        }
    }
}