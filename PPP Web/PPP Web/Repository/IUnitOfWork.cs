﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPP_Web.Repository
{
    public interface IUnitOfWork
    {
        int Save();
        Task<int> SaveAsync();
    }
}

