namespace PPP_Web.Migrations
{
    using PPP_Web.Models;
    using PPP_Web.Models.AuxData;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PPP_WebContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PPP_WebContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Schools.AddOrUpdate(
                p => p.Name,
                new School { Id = 1, Name = "Högstadiet" },
                new School { Id = 2, Name = "Gymnasium" }
                );

            context.Guests.AddOrUpdate(
                g => g.Id,
                new Guest { Gender = "F", SchoolId = 1 },
                new Guest { Gender = "M", SchoolId = 2 }
                    );

            context.Cities.AddOrUpdate(
                c => c.Id,
                new City() { Id = 1, Name = "Mars" }
                );

            context.Members.AddOrUpdate(
                m => m.Id,
                new Member() { FirstName = "Chuck", LastName = "Norris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Thuck", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Buck", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Smuck", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Luck", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Scmuck", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Schmuk", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Tuck", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Fruck", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" },
                new Member() { FirstName = "Puck", LastName = "Florris", Address = "Solsidan", PostCode = 8000, CityId = 1, BirthDate = DateTime.Parse("01-01-2000 13:07:48"), Gender = "M", Email = "jag@du.com", PhoneNr = "3640484", WantInfo = true, IceName = "Gud", IceRole = "Far", IcePhone = "077 777 777" }


                );

            context.MemberLogs.AddOrUpdate(
                l => l.Id,
                new Models.MemberLog() { MemberId = 1 },
                new Models.MemberLog() { MemberId = 3 }

                );
        }
    }
}
